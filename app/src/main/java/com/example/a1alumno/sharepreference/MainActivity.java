package com.example.a1alumno.a05_sharepreferecnces;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    SharedPreferences sharedPref;
    String name;
    int idName;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //COMMIT VARIABLES
        //SharedPreferences.Editor editor = getSharedPreferences("MyPrefsFile", MODE_PRIVATE).edit();
        //editor.putString("name", "Elena");
        //editor.putInt("idName", 12);
        //editor.commit();

        //Read VARIABLES
        SharedPreferences prefs = getSharedPreferences("MyPrefsFile", MODE_PRIVATE);
        name = prefs.getString("name", "No name defined");//"No name defined" is the default value.
        idName = prefs.getInt("idName", 0); //0 is the default value.

        Toast.makeText(getApplicationContext(), "name: " + name + " ID:" + idName, Toast.LENGTH_LONG).show();
    }
}